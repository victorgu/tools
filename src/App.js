import React from 'react';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>React Tools</h1>
        <ul>
          <li>Git flow</li>
          <li>Reduxjs/toolkit</li>
        </ul>
      </header>
    </div>
  );
}

export default App;
